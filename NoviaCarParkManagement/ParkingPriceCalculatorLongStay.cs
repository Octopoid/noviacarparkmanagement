﻿namespace NoviaCarParkManagement
{
    using System;
    using System.Globalization;
    using NoviaCarParkManagement.Extensions;
    using NoviaCarParkManagement.Interfaces;

    /// <summary>
    ///     The long stay parking pricing calculator. This is detailed in the specification on being a flat day rate on all
    ///     days. Partial days are charged in full.
    /// </summary>
    internal class ParkingPriceCalculatorLongStay : IParkingPriceCalculator
    {
        /// <summary>
        ///     A field which holds the current culture. This allows for payments to be made and displayed in other currencies.
        ///     Defaults to en-GB.
        /// </summary>
        private readonly CultureInfo cultureInfo;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ParkingPriceCalculatorLongStay" /> class.
        /// </summary>
        /// <param name="cultureInfo">The CultureInfo to use for this instance.</param>
        internal ParkingPriceCalculatorLongStay(CultureInfo cultureInfo = null)
        {
            this.cultureInfo = cultureInfo ?? CultureInfo.CreateSpecificCulture("en-GB");
        }

        /// <summary>
        ///     Gets or sets the day rate for staying in the car park in the local currency.
        /// </summary>
        public decimal DayRate { get; set; } = new decimal(7.50);

        /// <inheritdoc />
        public int CurrencyDecimalPlaces { get { return this.cultureInfo.NumberFormat.CurrencyDecimalDigits; } }

        /// <inheritdoc />
        public decimal Calculate(in DateTime arrived, in DateTime departed)
        {
            // Get the amount of billable days.
            int billableDays = this.GetBillableDays(arrived, departed);

            Output.Verbose($"Day Rate:       {this.DayRate.ToString("C", this.cultureInfo)}");
            Output.Verbose($"Billable Days:  {billableDays}");

            // The final stay cost is billable days multiplied by the day rate.
            decimal cost = billableDays * this.DayRate;

            return cost;
        }

        /// <summary>
        ///     Returns the number of billable days. This includes the start and end days, even if they are partial.
        /// </summary>
        /// <param name="arrived">The date the driver arrived at the parking location. This parameter cannot be modified.</param>
        /// <param name="departed">The date the driver departed from the parking location. This parameter cannot be modified.</param>
        /// <returns>The number of days, including partial days, in the range.</returns>
        private int GetBillableDays(in DateTime arrived, in DateTime departed)
        {
            // Strip the time off to ensure date range is complete.
            DateTime start = arrived.ToDayStart();
            DateTime end = departed.ToDayStart();

            return (end - start).Days + 1;
        }
    }
}
