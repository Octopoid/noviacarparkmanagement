﻿namespace NoviaCarParkManagement.Interfaces
{
    using System;
    using System.Globalization;

    /// <summary>
    ///     This interface defines a standardised pricing calculator which can be used for different stay types.
    /// </summary>
    public interface IParkingPriceCalculator
    {
        /// <summary>
        ///     Returns the number of decimal places in the current currency. This can be overriden in case a special pricing
        ///     structure is not using a <see cref="CultureInfo" /> object - for example, tokens.
        /// </summary>
        int CurrencyDecimalPlaces { get; }

        /// <summary>
        ///     Calculates the cost of the parking stay.
        /// </summary>
        /// <param name="arrived">The time the driver arrived at the parking location.</param>
        /// <param name="departed">The time the driver departed from the parking location.</param>
        /// <returns>The cost of the stay.</returns>
        decimal Calculate(in DateTime arrived, in DateTime departed);
    }
}
