﻿namespace NoviaCarParkManagement.Enumerations
{
    using System;
    using System.Globalization;
    using NoviaCarParkManagement.Interfaces;

    /// <summary>
    ///     An enumeration representing the different billing models that can be applied to a stay at the parking location.
    /// </summary>
    public enum StayType
    {
        /// <summary>
        ///     Short stay parking. Billing is £1.10 per hour (including partial hours), 8am to 6pm, Monday to Friday.
        /// </summary>
        Short,

        /// <summary>
        ///     Long stay parking. Billing is £7.50 per day, including partial days.
        /// </summary>
        Long
    }

    /// <summary>
    ///     Adds methods to the <see cref="StayType" /> enumerable.
    /// </summary>
    public static class StayTypeExtensions
    {
        /// <summary>
        ///     Returns a specific <see cref="IParkingPriceCalculator" /> for the given <see cref="StayType" />.
        ///     By providing a culture you can control which currency is displayed.
        /// </summary>
        /// <param name="stayType">The <see cref="StayType" /> to get the pricing calculator for.</param>
        /// <param name="cultureInfo">The <see cref="CultureInfo" /> to use. Controls the currency displayed.</param>
        /// <returns>Returns a specific concrete implementation for the <see cref="IParkingPriceCalculator" /> interface.</returns>
        public static IParkingPriceCalculator GetPriceCalculator(this StayType stayType, CultureInfo cultureInfo = null)
        {
            switch (stayType)
            {
                case StayType.Short:
                    return new ParkingPriceCalculatorShortStay(cultureInfo);
                case StayType.Long:
                    return new ParkingPriceCalculatorLongStay(cultureInfo);
                default:
                    throw new ArgumentOutOfRangeException(nameof(stayType), stayType, null);
            }
        }
    }
}
