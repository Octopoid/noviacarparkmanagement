﻿namespace NoviaCarParkManagement.Extensions
{
    using System;

    /// <summary>
    ///     Extends <see cref="DateTime" /> with helper methods.
    ///     Normally I would recommend using a library such as NodaTime to provide improved datetime handling.
    /// </summary>
    internal static class DateTimeExtensions
    {
        /// <summary>
        ///     Returns a date time which is constrained to be inside the end of a time range.
        ///     The date will remain unaffected, but the time will be limited to be no later than the provided time.
        /// </summary>
        /// <param name="input">The original input datetime. The date from here will be used.</param>
        /// <param name="hour">The hour of the latest allowed time.</param>
        /// <param name="minutes">The minute of the latest allowed time.</param>
        /// <param name="seconds">The second of the latest allowed time.</param>
        /// <param name="milliseconds">The milliseconds of the latest allowed time.</param>
        /// <returns>The original input date, modified where appropriate so the time is no later than the provided time.</returns>
        internal static DateTime GetTimePeriodEnd(this DateTime input,
                                                  int hour,
                                                  int minutes = 0,
                                                  int seconds = 0,
                                                  int milliseconds = 0)
        {
            DateTime periodEnd = input.ToDayStart().SetTime(hour, minutes, seconds, milliseconds);

            return input > periodEnd ? periodEnd : input;
        }

        /// <summary>
        ///     Returns a date time which is constrained to be inside the start of a time range.
        ///     The date will remain unaffected, but the time will be limited to be no earlier than the provided time.
        /// </summary>
        /// <param name="input">The original input datetime. The date from here will be used.</param>
        /// <param name="hour">The hour of the earliest allowed time.</param>
        /// <param name="minutes">The minute of the earliest allowed time.</param>
        /// <param name="seconds">The second of the earliest allowed time.</param>
        /// <param name="milliseconds">The milliseconds of the earliest allowed time.</param>
        /// <returns>The original input date, modified where appropriate so the time is no earlier than the provided time.</returns>
        internal static DateTime GetTimePeriodStart(this DateTime input,
                                                    int hour,
                                                    int minutes = 0,
                                                    int seconds = 0,
                                                    int milliseconds = 0)
        {
            DateTime periodStart = input.ToDayStart().SetTime(hour, minutes, seconds, milliseconds);

            return periodStart > input ? periodStart : input;
        }

        /// <summary>
        ///     Modifies the time component of a provided <see cref="DateTime" />.
        /// </summary>
        /// <param name="input">The <see cref="DateTime" /> object which should be returned with the modified time.</param>
        /// <param name="hour">The hour to apply to the input date.</param>
        /// <param name="minutes">The minute to apply to the input date.</param>
        /// <param name="seconds">The second to apply to the input date.</param>
        /// <param name="milliseconds">The millisecond to apply to the input date.</param>
        /// <returns>The supplied date with the newly supplied time.</returns>
        internal static DateTime SetTime(this DateTime input,
                                         int hour,
                                         int minutes = 0,
                                         int seconds = 0,
                                         int milliseconds = 0)
        {
            return new DateTime(input.Year,
                                input.Month,
                                input.Day,
                                hour,
                                minutes,
                                seconds,
                                milliseconds);
        }

        /// <summary>
        ///     Modifies the provided <see cref="DateTime" /> so the time is 23:59.59.999.
        /// </summary>
        /// <param name="input">The input <see cref="DateTime" /> to modify.</param>
        /// <returns>The provided <see cref="DateTime" /> with the modified time.</returns>
        internal static DateTime ToDayEnd(this DateTime input)
        {
            return new DateTime(input.Year,
                                input.Month,
                                input.Day,
                                23,
                                59,
                                59,
                                999);
        }

        /// <summary>
        ///     Modifies the provided <see cref="DateTime" /> so the time is 00:00.00.000.
        /// </summary>
        /// <param name="input">The input <see cref="DateTime" /> to modify.</param>
        /// <returns>The provided <see cref="DateTime" /> with the modified time.</returns>
        internal static DateTime ToDayStart(this DateTime input)
        {
            return new DateTime(input.Year, input.Month, input.Day);
        }
    }
}
