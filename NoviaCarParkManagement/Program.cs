﻿namespace NoviaCarParkManagement
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using CommandLineParser;
    using CommandLineParser.Arguments;
    using CommandLineParser.Exceptions;
    using NoviaCarParkManagement.Enumerations;

    public class Program
    {
        /// <summary>
        ///     A field containing the command line argument for the arrival date.
        /// </summary>
        private static ValueArgument<DateTime> argArrived;

        /// <summary>
        ///     A field containing the command line argument for the culture to use. Used to generate the
        ///     <see cref="cultureInfo" />. Defaults to en-GB.
        /// </summary>
        private static ValueArgument<string> argCulture;

        /// <summary>
        ///     A field containing the command line argument for the departure date.
        /// </summary>
        private static ValueArgument<DateTime> argDeparted;

        /// <summary>
        ///     A field containing the command line argument for the type of stay.
        /// </summary>
        private static EnumeratedValueArgument<StayType> argStayType;

        /// <summary>
        ///     A field containing the command line argument for the verbose flag. This is parsed and set to the
        ///     <see cref="verbose" /> field.
        /// </summary>
        private static SwitchArgument argVerbose;

        /// <summary>
        ///     The local culture to use. Allows foreign currencies to be used. Defaults to 'en-GB'.
        /// </summary>
        private static CultureInfo cultureInfo;

        /// <summary>
        ///     Contains the third party command line argument library. Simplifies accepting parameters in a variety of formats and
        ///     any order.
        /// </summary>
        private static CommandLineParser parser;

        /// <summary>
        ///     A field which controls whether verbose output is displayed to the end user.
        /// </summary>
        private static bool verbose;

        /// <summary>
        ///     Gets or sets the value of the culture field. This controls whether foreign currencies are used.
        ///     Defaults to 'en-GB'.
        /// </summary>
        public static CultureInfo CultureInfo { get { return Program.cultureInfo; } set { Program.cultureInfo = value; } }

        /// <summary>
        ///     Gets or sets the value of the verbose field. This controls whether verbose output is displayed to the end user.
        /// </summary>
        public static bool Verbose { get { return Program.verbose; } set { Program.verbose = value; } }

        /// <summary>
        ///     Main entry point for the application.
        /// </summary>
        /// <param name="args">The command line arguments supplied by the end user.</param>
        public static void Main(string[] args)
        {
            // DEBUG: Use to quickly set arguments if the program is run directly from the IDE.
            if (Debugger.IsAttached)
            {
                //args = new[] {"-s", "Short", "-a", "07/09/2017 16:50:00", "-d", "09/09/2017 19:15:00"};
                //args = new[] {"-s", "Short", "-a", "07/09/2017 16:50:00", "-d", "19/09/2017 11:15:00", "-v"};
                //args = new[] {"-s", "Long", "-a", "07/09/2017 16:50:00", "-d", "19/09/2017 11:15:00", "-v", "-c", "en-US"}; // show in dollars
            }

            // Parse the command line arguments.
            if (Program.ParseCommandLine(args))
            {
                StayType stayType = Program.argStayType.Value;
                DateTime arrived = Program.argArrived.Value;
                DateTime departed = Program.argDeparted.Value;

                Output.Verbose($"Stay Type:      {stayType}");
                Output.Verbose($"Arrived:        {arrived}");
                Output.Verbose($"Departed:       {departed}");
                Output.Verbose();

                try
                {
                    // Use the static API method to get the cost of the stay. This method will throw an InvalidOperationException if the date range is inverted.
                    // NOTE: The culture changes the displayed currency but handling an exchange rate is out of scope of this demo.
                    decimal stayCost = ParkingCharge.Calculate(stayType, arrived, departed, Program.cultureInfo);

                    Output.Verbose($"Parking Charge: {stayCost.ToString("C", Program.cultureInfo)}");
                    Output.Verbose();

                    if (!Program.Verbose)
                    {
                        Output.Write($"{stayCost.ToString("C", Program.cultureInfo)}");
                    }
                }
                catch (InvalidOperationException ex)
                {
                    // Quick simple debug method.
                    Output.Write($"Error: {ex.Message}");
                }
            }

            // If we're running this directly from the IDE, prevent the window from closing immediately.
            if (Debugger.IsAttached)
            {
                Console.ReadLine();
            }
        }

        /// <summary>
        ///     Sets up the command line argument parser and reads the values into the argument fields.
        /// </summary>
        /// <param name="args">The command line arguments to be parsed.</param>
        /// <returns>A boolean value indicating whether the arguments parsed successfully.</returns>
        private static bool ParseCommandLine(string[] args)
        {
            // Using an established Nuget package to deal with command line parsing.
            Program.parser = new CommandLineParser();

            // Example usage: ParkingCharge.exe -s Short -a "07/09/2017 16:50:00" -d "09/09/2017 19:15:00"
            ICollection<StayType> stayTypes = Enum.GetValues(typeof(StayType)).Cast<StayType>().ToList();
            Program.argStayType = new EnumeratedValueArgument<StayType>('s', "stay", "The type of billing to use for this stay.", stayTypes) {Optional = false};
            Program.argArrived = new ValueArgument<DateTime>('a', "arrived", "The time the vehicle arrived at the car park.") {Optional = false};
            Program.argDeparted = new ValueArgument<DateTime>('d', "departed", "The time the vehicle departed from the car park.") {Optional = false};
            Program.argVerbose = new SwitchArgument('v', "verbose", "Show a verbose display of how the calculation was performed.", false) {Optional = true};
            Program.argCulture = new ValueArgument<string>('c', "culture", "Set the culture to use to process the currency. Defaults to en-GB.") {Optional = true};

            // Uses the en-GB culture to parse dates. The specified culture is only used to display different currencies.
            Program.cultureInfo = CultureInfo.CreateSpecificCulture("en-GB");
            Program.argArrived.CultureInfo = Program.cultureInfo; // Use the given culture to parse the arrival date.
            Program.argDeparted.CultureInfo = Program.cultureInfo; // Use the given culture to parse the departure date.

            // Add the argument definitions to the parser
            Program.parser.Arguments.Add(Program.argStayType);
            Program.parser.Arguments.Add(Program.argArrived);
            Program.parser.Arguments.Add(Program.argDeparted);
            Program.parser.Arguments.Add(Program.argVerbose);
            Program.parser.Arguments.Add(Program.argCulture);

            // If no arguments are provided, show the usage guide.
            if (args == null || args.Length == 0)
            {
                Program.parser.ShowUsage();
                return false;
            }

            // Execute the parser and catch any errors.
            try
            {
                Program.parser.ParseCommandLine(args);

                // If another culture has been provided, recreate the CultureInfo object. This is passed along to the calculation call.
                if ((Program.argCulture != null) && Program.argCulture.Parsed && !string.Equals(Program.argCulture.Value, "en-GB", StringComparison.InvariantCultureIgnoreCase))
                {
                    Program.cultureInfo = CultureInfo.CreateSpecificCulture(Program.argCulture.Value);
                }
            }
            catch (MandatoryArgumentNotSetException) // TODO: Log exception.
            {
                Output.Write("Command line parameters not supplied correctly.");
                Program.parser.ShowUsage();

                // Did not parse correctly.
                return false;
            }
            catch (TargetInvocationException ex) // TODO: Log exception
            {
                if (ex.InnerException is FormatException)
                {
                    Output.Write("Date format appears to be incorrect. Are you using the correct culture?");
                }
                else
                {
                    Output.Write("Failed to parse the provided arguments.");
                }

                Program.parser.ShowUsage();

                // Did not parse correctly.
                return false;
            }

            // Loads the verbose argument into the normal bool field.
            Program.verbose = (Program.argVerbose != null) && Program.argVerbose.Parsed && Program.argVerbose.Value;

            // Parsed correctly.
            return true;
        }
    }
}
