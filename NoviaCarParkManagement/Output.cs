﻿namespace NoviaCarParkManagement
{
    using System;
    using System.Diagnostics;

    /// <summary>
    ///     Quick and simple helper class to enable verbose output. Would normally use an output stream instead, in a similar
    ///     way to Console, to allow for remapping to other outputs.
    /// </summary>
    internal static class Output
    {
        /// <summary>
        ///     Writes text out only if the <see cref="Program.Verbose" /> flag is set to true.
        ///     Displays text both in the console and the output window, if running from the IDE.
        /// </summary>
        /// <param name="line">The text to display if the condition is matched.</param>
        internal static void Verbose(string line = "")
        {
            if (Program.Verbose)
            {
                Console.WriteLine(line);
                Trace.WriteLine(line);
            }
        }

        /// <summary>
        ///     Writes text out both to the console and the output window, if running from the IDE.
        /// </summary>
        /// <param name="line">The text to display.</param>
        internal static void Write(string line = "")
        {
            Console.WriteLine(line);
            Trace.WriteLine(line);
        }
    }
}
