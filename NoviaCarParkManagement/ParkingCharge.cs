﻿namespace NoviaCarParkManagement
{
    using System;
    using System.Globalization;
    using NoviaCarParkManagement.Enumerations;
    using NoviaCarParkManagement.Interfaces;

    /// <summary>
    ///     Static class which provides the API functionality to generate parking charges.
    ///     This has been kept static as the calculation is a singular transaction. For the purposes of this example
    ///     application there does not appear to be a use case for storing or manipulating parking charges.
    /// </summary>
    public static class ParkingCharge
    {
        /// <summary>
        ///     Calculates the cost of a given parking stay in GBP.
        ///     If the provided date range is inverse this method will throw a <see cref="InvalidOperationException" />.
        /// </summary>
        /// <param name="stayType">
        ///     The stay type controls the type of billing which will be applied. This parameter cannot be
        ///     modified.
        /// </param>
        /// <param name="arrived">The date/time the driver arrived at the parking location. This parameter cannot be modified.</param>
        /// <param name="departed">The date/time the driver departed from the parking location. This parameter cannot be modified.</param>
        /// <param name="cultureInfo">
        ///     The culture to use for this request. This allows for foreign currencies to be used. Optional,
        ///     with a default value of 'en-GB'.
        /// </param>
        /// <returns>A <see cref="decimal" /> value representing the cost of the stay in the selected currency.</returns>
        public static decimal Calculate(in StayType stayType, in DateTime arrived, in DateTime departed, CultureInfo cultureInfo = null)
        {
            // Check for inverse date ranges.
            if (arrived > departed)
            {
                throw new InvalidOperationException("Arrival time must be before departure time.");
            }

            // Get the concrete implementation of the pricing calculator for the given stay type. Pass along any provided culture.
            IParkingPriceCalculator priceCalculator = stayType.GetPriceCalculator(cultureInfo);

            // Get the cost of the stay. Depending on the calculation, this figure may contain a large number of decimal places.
            decimal costRaw = priceCalculator.Calculate(arrived, departed);

            // Round the cost off as per the provided culture.
            decimal costRounded = Math.Round(costRaw, priceCalculator.CurrencyDecimalPlaces);

            return costRounded;
        }
    }
}
