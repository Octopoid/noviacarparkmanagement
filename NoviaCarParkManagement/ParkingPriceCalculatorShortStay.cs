﻿namespace NoviaCarParkManagement
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using NoviaCarParkManagement.Extensions;
    using NoviaCarParkManagement.Interfaces;

    /// <summary>
    ///     Calculates the price for a stay at the parking location using the short stay pricing model. This is calculated at
    ///     £1.10 an hour, 8am to 6pm on weekdays only. Partial hours are billed.
    /// </summary>
    internal class ParkingPriceCalculatorShortStay : IParkingPriceCalculator
    {
        /// <summary>
        ///     A field which holds the current culture. This allows for payments to be made and displayed in other currencies.
        ///     Defaults to en-GB.
        /// </summary>
        private readonly CultureInfo cultureInfo;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ParkingPriceCalculatorShortStay" /> class.
        /// </summary>
        /// <param name="cultureInfo">The CultureInfo to use for this instance.</param>
        internal ParkingPriceCalculatorShortStay(CultureInfo cultureInfo = null)
        {
            this.cultureInfo = cultureInfo ?? CultureInfo.CreateSpecificCulture("en-GB");
        }

        /// <summary>
        ///     Gets or sets the hour rate for staying in the car park in the local currency.
        /// </summary>
        public decimal HourRate { get; set; } = new decimal(1.10);

        /// <inheritdoc />
        public decimal Calculate(in DateTime arrived, in DateTime departed)
        {
            Output.Verbose("Billing applies Monday to Friday, 8am to 6pm.");
            Output.Verbose();

            // We calculate short stay pricing day by day, so we must track the total cost.
            decimal totalCost = 0;

            // Loop through each day - the DateRange method provides a Tuple of date time - start time, and end time.
            // This will always be on the same day, and is usually from 00:00-23:59. The start and end dates may be different times.
            foreach ((DateTime dayStart, DateTime dayEnd) in this.DateRange(arrived, departed))
            {
                // No billing applied at the weekend.
                if ((dayStart.DayOfWeek == DayOfWeek.Saturday) || (dayStart.DayOfWeek == DayOfWeek.Sunday))
                {
                    Output.Verbose($"{dayStart:dd/MM/yy ddd}: No billing applied.");

                    continue;
                }

                // Get a pair of date times which have been restricted to the billing period.
                // If the start time is after 8am or the end time is before 6pm, the billing period will be partial.
                DateTime billingStart = dayStart.GetTimePeriodStart(8);
                DateTime billingEnd = dayEnd.GetTimePeriodEnd(18);

                // Arrival range starts after billing has ended - skip this day.
                if (dayStart > billingEnd)
                {
                    Output.Verbose($"{dayStart:dd/MM/yy ddd}: Arrived {dayStart:t} - No billing applied.");

                    continue;
                }

                // Arrival range starts after billing has ended - skip this day.
                if (dayEnd < billingStart)
                {
                    Output.Verbose($"{dayStart:dd/MM/yy ddd}: Departed {dayEnd:t} - No billing applied.");

                    continue;
                }

                // Get the time span of the billable period for this day.
                TimeSpan billingRange = billingEnd - billingStart;

                // Get the billable hours - this is not rounded, so will apply partial hours.
                decimal billableHours = (decimal) billingRange.TotalHours;

                // Get the cost of this period by multiplying by the hourly rate.
                decimal cost = this.HourRate * billableHours;

                Output.Verbose($"{dayStart:dd/MM/yy ddd}: {billingStart:t}-{billingEnd:t} = {billableHours:F2} hours = {cost.ToString("C", this.cultureInfo)}");

                // Increment the total cost.
                totalCost += cost;
            }

            Output.Verbose();

            return totalCost;
        }

        /// <inheritdoc />
        public int CurrencyDecimalPlaces { get { return this.cultureInfo.NumberFormat.CurrencyDecimalDigits; } }

        /// <summary>
        ///     This iterator method enumerates through each day. The start and end dates include the provided times, but all other
        ///     dates are set to 00:00 and 23:59.
        /// </summary>
        /// <param name="arrived">The date the driver arrived at the parking location.</param>
        /// <param name="departed">The date the driver departed from the parking location.</param>
        /// <returns>An iterator which enumerates through each day in the range.</returns>
        private IEnumerable<Tuple<DateTime, DateTime>> DateRange(DateTime arrived, DateTime departed)
        {
            // Used to track when the end of the range has been hit.
            bool done = false;

            // Start the first day on the arrival date and time.
            DateTime dayStart = arrived;

            do
            {
                // Prevents date ranges that end on midnight overrunning by one day.
                if (dayStart >= departed)
                {
                    yield break;
                }

                // Set the day range to the end of the day. If this is the departure day, drop it back to the departure time.
                DateTime dayEnd = dayStart.ToDayEnd();

                if (dayEnd > departed)
                {
                    dayEnd = departed;
                    done = true;
                }

                // Would normally use something like NodaTime to get around the lack of range types for System.DateTime
                yield return new Tuple<DateTime, DateTime>(dayStart, dayEnd);

                // Move to 00:00 next morning.
                dayStart = dayStart.ToDayStart().AddDays(1);
            } while (!done);
        }
    }
}
