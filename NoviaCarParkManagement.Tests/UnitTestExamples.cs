namespace NoviaCarParkManagement.Tests
{
    using System;
    using System.Globalization;
    using NoviaCarParkManagement.Enumerations;
    using Xunit;

    public class UnitTestExamples
    {
        /// <summary>
        ///     Tests a range which falls entirely outside of a billable period. Starts at 8pm on a Friday, and ends at 5.45am on a
        ///     Monday. Final cost output should be �0.00.
        ///     <example>
        ///         A stay entirely outside of a chargeable period will return �0.00
        ///     </example>
        /// </summary>
        [Fact]
        public void TestExampleNone()
        {
            Program.Verbose = true;
            Assert.Equal(new decimal(0),
                         ParkingCharge.Calculate(StayType.Short,
                                                 new DateTime(2020, 04, 10, 20, 0, 0),
                                                 new DateTime(2020, 04, 13, 5, 45, 0)));
        }

        /// <summary>
        ///     Tests a range which ends before it begins. This operation should cause a <see cref="InvalidOperationException" />
        ///     to be raised.
        /// </summary>
        [Fact]
        public void TestExampleInverseRange()
        {
            Program.Verbose = true;
            Assert.Throws<InvalidOperationException>(() => ParkingCharge.Calculate(StayType.Short,
                                                 new DateTime(2020, 04, 13, 20, 0, 0),
                                                 new DateTime(2020, 04, 10, 5, 45, 0)));
        }

        /// <summary>
        ///     Example data as provided by the brief.
        ///     <example>
        ///         A short stay from 07/09/2017 16:50:00 to 09/09/2017 19:15:00 would cost �12.28
        ///     </example>
        /// </summary>
        [Fact]
        public void TestExampleShort()
        {
            Program.Verbose = true;
            Assert.Equal(new decimal(12.28),
                         ParkingCharge.Calculate(StayType.Short,
                                                 new DateTime(2017, 09, 07, 16, 50, 0),
                                                 new DateTime(2017, 09, 09, 19, 15, 0)));
        }

        /// <summary>
        ///     Example data as provided by the brief.
        ///     <example>
        ///         A long stay from 07/09/2017 07:50:00 to 09/09/2017 05:20:00 would cost �22.50
        ///     </example>
        /// </summary>
        [Fact]
        public void TestExampleLong()
        {
            Program.Verbose = true;
            Assert.Equal(new decimal(22.50),
                         ParkingCharge.Calculate(StayType.Long,
                                                 new DateTime(2017, 09, 07, 7, 50, 0),
                                                 new DateTime(2017, 09, 09, 5, 20, 0)));
        }

        /// <summary>
        ///     Tests a short range which falls entirely inside a single day, using the Short Stay pricing model. Final cost output should be �4.95.
        /// </summary>
        [Fact]
        public void TestExampleSingleDay()
        {
            Program.Verbose = true;
            Assert.Equal(new decimal(4.95),
                         ParkingCharge.Calculate(StayType.Short,
                                                 new DateTime(2020, 04, 7, 11, 15, 0),
                                                 new DateTime(2020, 04, 7, 15, 45, 0)));
        }

    }
}
